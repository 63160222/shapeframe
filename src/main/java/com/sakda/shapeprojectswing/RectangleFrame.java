/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sakda.shapeprojectswing;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author MSI GAMING
 */
public class RectangleFrame extends JFrame{
    JLabel lblRadius;
    JTextField txtRadius;
    JButton btnCalculate;
    JLabel lblResult;
    JTextField txtlong;
    JLabel lbllong;
    public RectangleFrame() {
        super("Rectangle");
        this.setSize(600, 600);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);
        
        lblRadius = new JLabel("wide", JLabel.TRAILING);
        lblRadius.setSize(50, 20);
        lblRadius.setLocation(5, 5);
        lblRadius.setBackground(Color.WHITE);
        lblRadius.setOpaque(true);
        this.add(lblRadius);
        
        lbllong = new JLabel("long", JLabel.TRAILING);
        lbllong.setSize(50, 20);
        lbllong.setLocation(5, 25);
        lbllong.setBackground(Color.WHITE);
        lbllong.setOpaque(true);
        this.add(lbllong);
        
        
        
        txtRadius = new JTextField();
        txtRadius.setSize(50, 20);
        txtRadius.setLocation(60, 5);
        this.add(txtRadius);
        
        txtlong = new JTextField();
        txtlong.setSize(50, 20);
        txtlong.setLocation(60, 25);
        this.add(txtlong);

        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        this.add(btnCalculate);
        
        lblResult = new JLabel("Rectangle wide= ??? long= ??? area= ??? perimeter =???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(500, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.ORANGE);
        lblResult.setOpaque(true);
        this.add(lblResult);
        
        btnCalculate.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
               String strRadius = txtRadius.getText();
               double w = Double.parseDouble(strRadius);
               String strlong = txtlong.getText();
               double l = Double.parseDouble(strlong);
               
               Rectangle rectangle = new Rectangle(w,l);
               lblResult.setText("Rectangle wide = "+ String.format("%.2f", rectangle.getW())+" long = "+String.format("%.2f", rectangle.getL())+" area = "+ String.format("%.2f", rectangle.calArea()) +" perimeter = "+String.format("%.2f", rectangle.calPerimeter()));
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(RectangleFrame.this,"Error: Please input number ", "Error", JOptionPane.ERROR_MESSAGE);
                    txtRadius.setText("");
                    txtRadius.requestFocus();
                    
                }
            }
        });
    }
    
    public static void main(String[] args) {
        RectangleFrame frame = new RectangleFrame();
        frame.setVisible(true);
        
    }
}