/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sakda.shapeprojectswing;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author MSI GAMING
 */
public class SquareFrame extends JFrame {

    JLabel lblside;
    JTextField txtside;
    JButton btnCalculate;
    JLabel lblResult;

    public SquareFrame() {
        super("Square");
        this.setSize(300, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblside = new JLabel("side", JLabel.TRAILING);
        lblside.setSize(50, 20);
        lblside.setLocation(5, 5);
        lblside.setBackground(Color.WHITE);
        lblside.setOpaque(true);
        this.add(lblside);

        txtside = new JTextField();
        txtside.setSize(50, 20);
        txtside.setLocation(60, 5);
        this.add(txtside);

        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        this.add(btnCalculate);

        lblResult = new JLabel("Square side= ??? area= ??? perimeter= ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(300, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.ORANGE);
        lblResult.setOpaque(true);
        this.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strside = txtside.getText();
                    double side = Double.parseDouble(strside);
                    Square square = new Square(side);
                    lblResult.setText("Square s = " + String.format("%.2f", square.getSide()) + " area = " + String.format("%.2f", square.calArea()) + " perimeter = " + String.format("%.2f", square.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(SquareFrame.this, "Error: Please input number ", "Error", JOptionPane.ERROR_MESSAGE);
                    txtside.setText("");
                    txtside.requestFocus();

                }
            }
        });
    }
     public static void main(String[] args) {
       SquareFrame frame = new SquareFrame();
        frame.setVisible(true);
    }
}

