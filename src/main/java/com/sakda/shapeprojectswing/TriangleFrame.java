/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sakda.shapeprojectswing;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author MSI GAMING
 */
public class TriangleFrame extends JFrame {

    JLabel lblhigh;
    JTextField txthigh;
    JButton btnCalculate;
    JLabel lblResult;
    JTextField txtbase;
    JLabel lblbase;

    public TriangleFrame() {
        super("Triangle");
        this.setSize(600, 600);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblhigh = new JLabel("base", JLabel.TRAILING);
        lblhigh.setSize(50, 20);
        lblhigh.setLocation(5, 5);
        lblhigh.setBackground(Color.WHITE);
        lblhigh.setOpaque(true);
        this.add(lblhigh);

        lblbase = new JLabel("high", JLabel.TRAILING);
        lblbase.setSize(50, 20);
        lblbase.setLocation(5, 25);
        lblbase.setBackground(Color.WHITE);
        lblbase.setOpaque(true);
        this.add(lblbase);

        txtbase = new JTextField();
        txtbase.setSize(50, 20);
        txtbase.setLocation(60, 5);
        this.add(txtbase);

        txthigh = new JTextField();
        txthigh.setSize(50, 20);
        txthigh.setLocation(60, 25);
        this.add(txthigh);

        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        this.add(btnCalculate);

        lblResult = new JLabel("Triangle base= ??? high= ??? area= ??? perimeter =???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(500, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.ORANGE);
        lblResult.setOpaque(true);
        this.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strbase = txtbase.getText();
                    double b = Double.parseDouble(strbase);
                    String strhigh = txthigh.getText();
                    double h = Double.parseDouble(strhigh);

                   Triangle triangle = new Triangle(b, h);
                    lblResult.setText("Triangle base = " + String.format("%.2f", triangle.getB()) + " high = " + String.format("%.2f", triangle.getH()) + " area = " + String.format("%.2f", triangle.calArea()) + " perimeter = " + String.format("%.2f", triangle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(TriangleFrame.this, "Error: Please input number ", "Error", JOptionPane.ERROR_MESSAGE);
                    txtbase.setText("");
                    txtbase.requestFocus();
                    txthigh.setText("");
                    txthigh.requestFocus();
                    

                }
            }
        });
    }
    public static void main(String[] args) {
        TriangleFrame frame = new TriangleFrame();
        frame.setVisible(true);
        
    }
}
