/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sakda.shapeprojectswing;

/**
 *
 * @author MSI GAMING
 */
public class Triangle extends Shape {

    private double b;
    private double h;
    private double a;

    public Triangle(double b, double h) {
        super("Triangle");
        this.b = b;
        this.h = h;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    public double getH() {
        return h;
    }

    public void setH(double h) {
        this.h = h;
    }

    @Override
    public double calArea() {
        a = 0.5 * b * h;
        return a;
    }

    @Override
    public double calPerimeter() {
        return a + b + h;
    }
}
