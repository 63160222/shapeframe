/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sakda.shapeprojectswing;

/**
 *
 * @author MSI GAMING
 */
public class Square extends Shape{
    double side;

    public Square(double side) {
        super("Square");
        this.side = side;
    }

    public double getSide() {
        return side;
    }

    @Override
    public double calArea() {
        return side * side;
    }

    @Override
    public double calPerimeter() {
        return 4*side;
    }
    
}
